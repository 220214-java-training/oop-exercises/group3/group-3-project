package veh;

public class Cars {


    public static void main(String[] args) {
        Bmw b = new Bmw("Blue", "Bmw", "i8", "luxury");
        Honda h = new Honda("White", "Honda", "Accord", "Economy");
        Ford f = new Ford("Black", "Ford", "Mustang", "Sports");

        System.out.println(b.color + ", " + b.model + ", " + b.make + ", " + b.type);
        b.drive();
        System.out.println(h.color + ", " + h.model + ", " + h.make + ", " + h.type);
        f.drive();
        System.out.println(f.color + ", " + f.model + ", " + f.make + ", " + f.type);
        h.drive();

    }
}
