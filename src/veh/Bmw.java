package veh;

public class Bmw extends Vehicle {

    Bmw() {

        super();
    }
    Bmw(String bmwColor, String bmwModel, String bmwMake, String bmwType) {

        super(bmwColor, bmwModel, bmwMake, bmwType);
    }

    void drive() {
        System.out.println("vroom vroom!.... I have a very expensive engine so I require premium gas!");

    }
}