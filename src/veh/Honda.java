package veh;

public class Honda extends Vehicle {

    Honda() {

        super();
    }

    Honda(String hColor, String hModel, String hMake, String hType) {
        super(hColor, hModel, hMake, hType);

    }

    void drive() {
        System.out.println("Driving slowly ... because i am a honda and i save gas.");

    }

}

