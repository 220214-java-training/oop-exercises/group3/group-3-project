package veh;

public abstract class Vehicle {

    String color;
    String model;
    String make;
    String type;

    Vehicle(String color, String model, String make, String type) {
        super();
        this.color = color;
        this.model = model;
        this.make = make;
        this.type = type;

    }

    public Vehicle() {


    }

    void drive() {
        System.out.println("We all go fast!");

    }
}

